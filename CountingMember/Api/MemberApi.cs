﻿using CountingMember.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CountingMember.Share;

namespace CountingMember.Api
{

    [Route("api/[controller]")]
    [ApiController]
    public class MemberController : ControllerBase
    {
        private IMemberService memberService;

        public MemberController(IMemberService memberService)
        {
            this.memberService = memberService;
        }

        // GET api/values
        [HttpGet("load")]
        public JsonResult LoadDatabase()
        {
            var memberTotal = memberService.countMember();
            return new JsonResult(new { memberTotal = memberTotal });
        }
        //total
        [HttpGet("total")]
        public JsonResult LoadCurrentTotal()
        {
            return new JsonResult(new { memberTotal = Setting.MEMBER_TOTAL });
        }
        [HttpPut("{score}")]
        [Consumes("application/x-www-form-urlencoded")]
        public void SendScore(int score)
        {
            Setting.MEMBER_TOTAL = score;
        }

        [HttpGet]
        public JsonResult GetScore()
        {
            return new JsonResult(new { memberTotal = Setting.MEMBER_TOTAL.ToString("D6") } );
        }
    }
}
