﻿using CountingMember.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountingMember.Services
{
    public class MemberService : IMemberService
    {
        private LineMemberContext db;

        
        public MemberService(LineMemberContext db)
        {
            this.db = db;
        }

        public int countMember()
        {
            return db.User.Count();
        }
    }
}
