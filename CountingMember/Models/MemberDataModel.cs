﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountingMember.Models
{
    public class MemberDataModel
    {   
        public int memberTotal { get; set; }

        //Setting
        public bool turnOn { get; set; }
        public int delay_ms { get; set; }
        public int timer_ms { get; set; }

        //Data
        public int currentTotal { get; set; }
        public int maxTotal { get; set; }
    }
}
