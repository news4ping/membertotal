﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CountingMember.Models
{
    public interface IMemberService
    {
        int countMember();
    }
}
